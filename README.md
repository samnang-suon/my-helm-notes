# 00h49m_LYNDA_Kubernetes Package Management with Helm 2020
![PackageManagerChart](images/PackageManagerChart.png)

## Without Helm
![WithoutHelm](images/WithoutHelm.png)

## Installation for Ubuntu

    curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
    sudo apt-get install apt-transport-https --yes
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm
Reference:
https://helm.sh/docs/intro/install/#from-apt-debianubuntu

Q: Where can you find package for you kubernetes cluster ?  
A: on the Helm hub website: https://artifacthub.io/

# ==================================================
# 01h19m_PLURALSIGHT_Kubernetes Package Administration with Helm 2020 andrew Pruski
![WhatAreCharts01](images/WhatAreCharts01.png)

![WhatAreCharts02](images/WhatAreCharts02.png)

## Helm Commands Overview
![HelmKeyCommands](images/HelmKeyCommands.png)
